function x = getdist(A, B)

[unk, sz_a] = size(A);
x = 0;



for i = 1:sz_a
    x = x + ((A(i) - B(i))^2);
end

x = sqrt(x);

