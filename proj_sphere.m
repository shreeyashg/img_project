function x = proj_sphere(c_center, c_radius, p_point)

[m, n] = size(c_center);
vec = (p_point - c_center);
naaaa = norm(vec, 'fro');

k = (vec/naaaa) * c_radius;

x = k + c_center;

if naaaa < c_radius
    x = p_point;
end