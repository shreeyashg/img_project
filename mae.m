function x = mae(F, ref)

[m, n] = size(F);

s = sum(sum(abs(F - (ref))));
x = s/(m * n);