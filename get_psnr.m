function psnr = get_psnr(im1, im2)

% Find MSE and then you can go for PSNR & SNR.
% For the gray image (0 to 255), convert both the image to double.
im1 = double(im1);
im2 = double(im2);
% Find the mean squared error
mse = sum((im1(:)-im2(:)).^2) / prod(size(im1));
% now find the psnr, as peak=255
psnr = 10*log10(255*255/mse);
 
