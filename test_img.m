jfunction x = test_img(F)

sigma = 1;
blurry = blur_gaussian_2D(F, sigma);
original = F;
noisy = imnoise(F, 'gaussian', 0.01);
noisy_blurred = blur_gaussian_2D(noisy, sigma);

fprintf('Using the L2norm function: \n');
fprintf('Original = %.4f, Noisy = %.4f, Blurry = %.4f, Noisy_blurred = %.4f \n', L2norm(original), L2norm(noisy), L2norm(blurry), L2norm(noisy_blurred));
fprintf('Using the ROF function: \n');
fprintf('Original = %.4f, Noisy = %.4f, Blurry = %.4f, Noisy_blurred = %.4f \n', ROF(original), ROF(noisy) ,ROF(blurry), ROF(noisy_blurred));
%disp(L2norm(original));
%disp('Blurry');
%disp(L2norm(blurry));