\beamer@sectionintoc {1}{Introduction}{2}{0}{1}
\beamer@subsectionintoc {1}{1}{About Image Noise}{2}{0}{1}
\beamer@subsectionintoc {1}{2}{Example of Good Image}{4}{0}{1}
\beamer@subsectionintoc {1}{3}{Example of Noisy Image}{5}{0}{1}
\beamer@subsectionintoc {1}{4}{The Metrics}{6}{0}{1}
\beamer@sectionintoc {2}{Naive Methods}{7}{0}{2}
\beamer@subsectionintoc {2}{1}{Using Gaussian Blur}{7}{0}{2}
\beamer@subsectionintoc {2}{2}{Noisy Image}{9}{0}{2}
\beamer@subsectionintoc {2}{3}{Blurred Version of Noisy Image}{10}{0}{2}
\beamer@sectionintoc {3}{Problem Formulation}{11}{0}{3}
\beamer@subsectionintoc {3}{1}{The Problem}{11}{0}{3}
\beamer@subsectionintoc {3}{2}{Some Insights}{12}{0}{3}
\beamer@sectionintoc {4}{The Algorithm}{13}{0}{4}
\beamer@subsectionintoc {4}{1}{Pseudocode}{13}{0}{4}
\beamer@sectionintoc {5}{Results}{14}{0}{5}
\beamer@subsectionintoc {5}{1}{PSNR, MAE Table}{14}{0}{5}
\beamer@subsectionintoc {5}{2}{Noisy Image (Aerial)}{15}{0}{5}
\beamer@subsectionintoc {5}{3}{Denoised Image (Aerial)}{16}{0}{5}
\beamer@subsectionintoc {5}{4}{Noisy Image (Barbara)}{17}{0}{5}
\beamer@subsectionintoc {5}{5}{Denoised Image (Barbara)}{18}{0}{5}
\beamer@subsectionintoc {5}{6}{Noisy Image (Cameraman)}{19}{0}{5}
\beamer@subsectionintoc {5}{7}{Denoised Image (Cameraman)}{20}{0}{5}
\beamer@sectionintoc {6}{Further Work}{21}{0}{6}
\beamer@subsectionintoc {6}{1}{Scope for Improvements}{21}{0}{6}
\beamer@sectionintoc {7}{Acknowledgements}{22}{0}{7}
\beamer@sectionintoc {8}{The End}{23}{0}{8}
