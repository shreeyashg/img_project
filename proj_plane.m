function x = proj_plane(p_point, v_normal, proj_point)

[m, n] = size(p_point);

ad = 0;
sq = 0;
ax = 0;

for i = 1:n
    ad = ad + (p_point(i) * v_normal(i));
    ax = ax + (proj_point(i) * v_normal(i));
    sq = sq + (v_normal(i) * v_normal(i));
end

t = (ad - ax) / sq;
x = zeros(1, n);

for i = 1:n
    x(i) = ( proj_point(i) + (v_normal(i) * t) );
end