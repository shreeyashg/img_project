function fimg = PDHGM(F, K, var, sigma, rho, og)

tic
F = sparse(F);
og = sparse(og);
[m, n] = size(F);
lambda = m * n * var;
L_x = sparse(L(m));
L_y = sparse(L(n));
u = sparse(zeros(m, n));
p1 = sparse(u);
p1d = sparse(u);
p2x = sparse(u);
p2y = sparse(u);
p2dx = sparse(u);
p2dy = sparse(u);

peaksnr1 = 0.0;
maxw = 0;
fimg = u;

for k = 1:K
    u = min(1, max(0, u - sigma * rho * (p1d + L_x' * p2dx + p2dy * L_y)));
    v1 = sparse(proj_sphere(F, sqrt(lambda), u + p1));
    Dx = sparse(p2x + L_x * u); % The x-component of the Gradient
    Dy = sparse(p2y + u * L_y');
    v2x = sparse(Dx/(1 + (2. / sigma))); % x 
    v2y = sparse(Dy/(1 + (2. / sigma))); % y
    p1k = (p1), p2kx = (p2x), p2ky = (p2y);
    p1 = p1 + u - v1;
    p2x = p2x + L_x * u - v2x, p2y = p2y + u * L_y' - v2y;
    p1d = p1 + (p1 - p1k);
    p2dx = p2x + (p2x - p2kx), p2dy = p2y + (p2y - p2ky);
    peaksnr = psnr(full(u), full(og)); 

    a1 = num2str(peaksnr);
    b2 = num2str(peaksnr1);
    if (isequal(a1, b2))
        cnt = cnt + 1;
    else
        cnt = 0;
    end
    
    if (cnt == 50)
        break;
    end
    str = strcat('Iteration_', num2str(k), ' PSNR= ',  num2str(peaksnr));
    if (peaksnr > maxw)
        maxw = peaksnr;
        fimg = u;
    end
    peaksnr1 = peaksnr;
    disp(str);
end

[peaksnr, snr] = psnr(full(u), full(og));
[peaksnr_n, snr_n] = psnr(full(F), full(og));

toc