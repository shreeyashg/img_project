function x = ROF(F)

x = 0;
[m, n] = size(F);
G = Grad(F);

for i = 1:m
    for j = 1:n
        x = x + G(i, j);
    end
end

