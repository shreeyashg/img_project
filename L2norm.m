function x = L2norm(F)

x = 0;
G = Grad(F);

[m, n] = size(G);

for i = 1:m
    for j = 1:n
        x = x + (G(i, j)^2);
    end
end


