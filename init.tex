\documentclass[12pt]{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath, amsthm, amssymb} % math
\usepackage{graphicx} % pictures
\usepackage{float} % picture placement
\usepackage{fullpage}
\usepackage{hyperref}
\usepackage{caption}
\usepackage{titlesec}
\usepackage[table]{xcolor}
\usepackage{amsmath,amsfonts}


\DeclareMathSizes{20}{20}{20}{20}


\providecommand{\keywords}[1]{\textbf{\textit{Keywords:}} #1}

\title{\bf{Denoising Images with Optimized Primal-Dual Hybrid Gradient Algorithms}}    
\date{}

\author{Shreeyash Gotmare \\ PACE Junior Science College, Andheri \\ Mumbai, India \\ \href{mailto:shreeyashg@hotmail.com}{ shreeyashg@hotmail.com} }


\begin{document}
\maketitle

\begin{abstract}
Image processing [1] is used in almost every sub-domain of modern science and engineering. The images captured have several features in them, some of which are characteristic of the media used to capture the image. Noise in images is one of such features. For example, when you capture an image using your camera, you do not see the raw image; instead, you see a denoised version of the raw image. In this paper, we quantify the noise in our image as a convex function and use a convex optimization technique to minimize this function. Hence, denoising our image. To minimize the convex function we use primal-dual hybrid gradient algorithms [2] with several improvements. We then discuss the improvements in denoising using PSNR (peak signal-to-noise ratio) between the images. \\
\end{abstract}

\keywords{image denoising, primal-dual algorithms, gaussian/poisson filters, convex optimization}


\begin{figure}
\begin{minipage}[t]{0.49\linewidth}
\centering
\includegraphics[width=\linewidth]{cameraman.jpg}

(a) True Image
\end{minipage}%
\hfill\vrule\hfill
\begin{minipage}[t]{0.49\linewidth}
\centering
\includegraphics[width=\linewidth]{noisy_cameraman.jpg}

(b) with Gaussian Noise of $\mu = 0 $ and $\sigma = 0.01$
\end{minipage}
\caption{Comparison of images with and without noise}
\end{figure}


\section{Motivation}

The images we capture have some kind of noise in them. Capturing an image without noise is impractical because of the features of the capturing media. Therefore, we use denoising algorithms for post-processing of the image and creating a denoised image. \vspace{2mm}

In medical imaging, radiography and anthropology, denoising images is crucial in identifying the distinguishing features of the image. Denoising algorithms [3] play an important role here. \vspace{2mm} 

The most common and naïve method for denoising images is applying Gaussian blur on the image. Gaussian blur works by taking a weighted average of the neighbouring pixels using the Gaussian function. The equation of the Gaussian function in one dimension is $$ G(x) = \frac{1}{\sqrt{2\pi \sigma^2}} e^{-\frac{x^2}{2 \sigma^2}} $$ In two dimensions, the equation is a product of two such Gaussians, one in each dimension $$ G(x,y) = \frac{1}{{2\pi \sigma^2}} e^{-\frac{x^2 + y^2}{2 \sigma^2}} $$ A kernel is defined as the set of some neighbouring pixels for a pixel. For each pixel, the kernel spans a contiguous region in its neighbourhood and takes a weighted average of the pixels with the closest pixel having the highest weight and farthest pixel having the least weight, according to the Gaussian equation. The size of the kernel may be changed by changing the value of $ \sigma $ in the equation.  \vspace{2mm}

It is apparent that blurring would always lead to loss of information in the image. It is very easily noticeable that we lose a lot of edge contrast while blurring. Thus, we need more efficient and accurate algorithms for image denoising. \vspace{2mm}


\begin{figure}
  \centering
  \includegraphics[scale = 0.165]{blurry_cameraman.jpg}
  \caption{Gaussian blur on above noisy image (with $\sigma = 2$)}
\end{figure}

\section{Notation and Preliminaries}

In this section, we describe the prerequisites for understanding our algorithm. We represent the image as a two dimensional matrix with the intensity of a pixel at each position in the matrix. We denote the size of this grid as $ m \times n $ where $m$ is the horizontal component and $n$ is the vertical component. \\

\subsection{Grayscale Image \\}

A grayscale image is an image in which the value of each pixel is a single sample, that is, it carries only intensity information. Images of this sort, also known as black-and-white, are composed exclusively of shades of gray, varying from black at the weakest intensity to white at the strongest. The intensity of a pixel is expressed within a given range between a minimum and a maximum, inclusive. This range is represented in an abstract way as a range from 0 (total absence, black) and 1 (total presence, white), with any fractional values in between. This notation is used in academic papers, but this does not define what "black" or "white" is in terms of colorimetry. \\

\subsection{Gaussian Noise \\}

Gaussian noise is statistical noise [2] having a probability density function (PDF) equal to that of the normal distribution, which is also known as the Gaussian distribution. In other words, the values that the noise can take on are Gaussian-distributed. \vspace{2mm} The probability density function $ p $ of a Gaussian random variable $ z $ is given by $$ p_G(z) = \frac{1}{\sigma\sqrt{2\pi}} e^{ -\frac{(z-\mu)^2}{2\sigma^2} } $$ where $z$ represents the grey level, $\mu$ the mean value and $\sigma$ the standard deviation. \\

\subsection{Image Gradients \\}

Image gradients can be used to extract information from images. Gradient images are created from the original image for this purpose. Each pixel of a gradient image measures the change in intensity of that same point in the original image, in a given direction. To get the full range of direction, gradient images in the x and y directions are computed. If we assume the gradient image as $G$ and the original image as $F$, then we define the intensity at position $(i, j)$ as

 $$ G(i, j) = \sqrt{((F(i + 1, j) - F(i, j))^2 + (F(i, j + 1) - F(i, j))^2)} $$ \
The gradient image helps us easily identify edges in the original image.


\begin{figure}
\begin{minipage}[t]{0.49\linewidth}
\centering
\includegraphics[width=\linewidth]{grad_cameraman.jpg}
Gradient image of noise-free cameraman
\end{minipage}%
\hfill\vrule\hfill
\begin{minipage}[t]{0.49\linewidth}
\centering
\includegraphics[width=\linewidth]{grad_noisy_cameraman.jpg}
Gradient image of cameraman with Gaussian noise having $\sigma $ = 0.01
\end{minipage}
\caption{Gradient image comparison}
\end{figure}



\subsection{Peak Signal-to-Noise Ratio (PSNR)\\}

PSNR is most commonly used to measure the quality of reconstruction of lossy compression codecs (e.g., for image compression). The signal in this case is the original data, and the noise is the error introduced by compression. When comparing compression codecs, PSNR is an approximation to human perception of reconstruction quality. Although a higher PSNR generally indicates that the reconstruction is of higher quality, in some cases it may not. We use PSNR as a metric to rank our images.

PSNR is most easily defined via the mean squared error (MSE). Given a noise-free $m \times n$ monochrome image $I$ and its noisy approximation $K$, $MSE$ is defined as: $${MSE} = \frac{1}{m\,n}\sum_{i=0}^{m-1}\sum_{j=0}^{n-1} [I(i,j) - K(i,j)]^2     $$ The PSNR (in dB) is now defined as 
\begin{align*}
\mathit{PSNR} &= 10 \cdot \log_{10} \left( \frac{\mathit{MAX}_I^2}{\mathit{MSE}} \right)\\ 
&= 20 \cdot \log_{10} \left( \frac{\mathit{MAX}_I}{\sqrt{\mathit{MSE}}} \right)\\ 
&= 20 \cdot \log_{10} \left( {\mathit{MAX}_I} \right) - 10 \cdot \log_{10} \left( {{\mathit{MSE}}} \right)
\end{align*}
\begin{figure}
\begin{minipage}[t]{0.49\linewidth}
\centering
\includegraphics[width=\linewidth]{noisy_lena.jpg}

Lena, PSNR = 20.0675 dB
\end{minipage}%
\hfill\vrule\hfill
\begin{minipage}[t]{0.49\linewidth}
\centering
\includegraphics[width=\linewidth]{u_lena.jpg}

(b) Lena, PSNR = 27.3055 dB
\end{minipage}
\caption{Comparison of the PSNR of images}
\end{figure}


\section{Mathematical Model \\}

We are trying to minimize the following function $$\bar{u} = \displaystyle argmin_{\enspace 0 \leq u \leq 1 \enspace} \sum_{i = 1}^{m} \sum_{j = 1}^{n} (\nabla_{x_{ij}}^{2} + \nabla_{y_{ij}}^{2}) $$ such that the following inequality remains true $$\sum_{i = 1}^{m} \sum_{j = 1}^{n} (u_{ij} - f_{ij})^2 \leq m * n * \sigma^2$$ where $f$ is our noisy image and $\sigma^2$ is the variance of Gaussian noise in our image. \\ This is a convex function so we can use convex optimization techniques to minimize the function the above constraint. The other mathematical techniques we use are described below. 

\subsection{Sampled Variance \\}

The sampled variance helps approximate the variance of large images. It takes $\mathcal{O}(1)$ time to find the variance of the image using the sampled variance. The value of the sampled variance is usually very close to the true variance.
It is simply $$m \times n \times \sigma^2$$ where $\sigma^2$ is the variance of the Gaussian noise in the image.

\subsection{The L - matrices}

We use two matrices $L_x $ and $L_y$. The matrix $L_x$ will be a square matrix of size $m \times m$. The matrix $L_y$ will be a square matrix of size $n \times n$. We generate them using the formula $$L_x(i, i) = -1, \enspace L_x(i, i + 1) = 1, \enspace \forall \enspace 0 \leq i \leq m - 1 $$ $$ L_y(i, i) = -1, \enspace L_y(i, i + 1) = 1, \enspace \forall \enspace 0 \leq i \leq n - 1  $$


\section{The Algorithm}

The algorithm[1] can be mainly divided into five steps. We represent all the intermediate variables as image matrices. The algorithm has five inputs which can be summarized as follows:

\begin{itemize}
\item $F$, the input image of size $m \times n$.
\item $K$, the upper bound on the number of iterations of our algorithm.
\item $var$, the variance of Gaussian noise in the image.
\item $\sigma$ and $\rho$, parameters which optimize the convergence of our algorithm.
\end{itemize}

\subsection{Step 0: Initialize all the matrices}

We create seven matrices: $u_0 $, $p_1$, $p_1d$, $p_2x$, $p_2y$, $p_2dx$, $p_2dy$ and initialize all of them to zero matrices of the size of the input image ($m \times n$). Now we iterate from step 1 to step 5 $K$ times. Also, we define $\bar{i}$ to be equal to $m * n * \sigma^2$ (or the sampled variance).

\subsection{Step 1: Compress the pixel values to $[0, 1]$}

In the $i$th iteration, $$u_i = min(1, max(0, u - \sigma * \rho * (p_1d + L_x' * p_2dx + p_2dy * L_y) )))$$
\subsection{Step 2: Orthogonal Projection}

We perform an orthogonal projection onto an $\mathcal{N}$-dimensional ball centered at the input image $F$ and having radius $\sqrt{\bar{i}}$ and store it as the matrix $v_1$. $$v_1 = P_{B_\mathcal{N}(F, \sqrt{\bar{i}})}(u_i + p_1)$$ where $P_{B_\mathcal{N}(C, R)}\mathcal{X}$ implies the orthogonal projection of the point $\mathcal{X}$ on the $\mathcal{N}$-dimensional ball centered at the point $C$ having radius $R$.

\subsection{Step 3: Proximation Mapping}

For a proper, convex function $f$, the proximation mapping is defined by $$Prox_{\lambda f}(x) := argmin\{\frac{1}{2\lambda} ||x - y||^2_2 + f(y)\}$$ In our algorithm, we use the $\ell^2$ norm as a cost function and using its proximation mapping, we minimize the function. For the $\ell^2$ norm, we know that the cost function $$g(x_1, x_2) = x_1^2 + x_2^2$$ Now we can find the proximation mapping in the following way: $$Prox_{\lambda f}(x) = argmin\{\frac{1}{2\lambda} ((x_1 - y_1)^2 + (x_2 - y_2)^2)  + (y_1^2 + y_2^2)\}$$ Now we differentiate this function with respect to $y_1$ $$ \frac{\partial}{\partial y_1} = \frac{1}{2\lambda} (0 - 2x_1 + 2y_1) + 2y_1$$ With respect to $y_2$ $$ \frac{\partial}{\partial y_2} = \frac{1}{2\lambda}(0 - 2x_2 + 2y_2) + 2y_2$$ After setting these equations to be equal to zero and appropriate transformations, we get $$ y_1 = \frac{x_1}{1 + 2\lambda}, \enspace y_2 = \frac{x_2}{1 + 2\lambda}  $$ These are the minima of our convex function.  Now, in our algorithm, we create two matrices $D_x$ and $D_y$ and initialize them in this manner $$D_x = p_2x + L_x * u$$ $$D_y = p_2y + u * L_y'$$ Now we create another two matrices, and simply initialize them as $$v_{2_x} = \frac{D_x}{1 + \frac{2}{\sigma}}, \enspace v_{2_y} = \frac{D_y}{1 + \frac{2}{\sigma}}$$

\subsection{Step 4: Arithmetic}

Now we just do simple arithmetic with the intermediate images (or matrices) that we have obtained and set the values for $p_1$, $p_{2_x}$ and $p_{2_y}$.

$$p_1 = u - v_1$$
$$p_{2_x} = p_{2_x} + L_x * u - v_{2_x}$$
$$p_{2_y} = p_{2_y} + u * L_y' - v_{2_y}$$

\subsection{Step 5: More Arithmetic}

We now obtain the values for $p_1d$, $p_2dx$ and $p_2dy$ using the following equations $$p_1d = p_1d + (p_1 - p_{1_k})$$ $$p_2dx = p_2x + (p_2x - p_{2_k}x)$$ $$p_2dy = p_2y + (p_2y - p_{2_k}y)$$ where a $k$ in the subscript denotes the matrix from the previous iteration. \\ \\
We repeat the above steps for $K$ iterations or until the value of $u_i$ converges.

\section{Optimizing the Algorithm}

Naively, our Algorithm should run in $\mathcal{O}(K * m^3)$ time where K is the number of iterations and m is the side of the square of the matrix. This is since, in every iteration, the complexity is dominated by that of matrix multiplication which is naively $\mathcal{O}(m^3)$. Therefore, the total runtime of our algorithm is $\mathcal{O}(K * m^3)$.
 Some of the approaches to decrease the complexity/constant factors of our algorithm are as follows:

\begin{itemize}

\item Using the Coppersmith–Winograd algorithm [4] for matrix multiplication. The Coppersmith–Winograd method can multiply two $m \times m$ matrices in $\mathcal{O}(m^{2.375})$ as compared to the naïve $\mathcal{O}(m^3)$. This is an immense improvement to our algorithm.

\item Using \textit{sparse} matrices in MATLAB [5] for matrix calculations. This also improves our memory complexity and shaves off several constant factors from our complexity.

\item Addition of a convergence check to our algorithm and halting it before $K$ iterations. We can check the difference between $u_{i + 1}$ and $u_i$ and if it is less than some threshold $\varepsilon$ we can halt  the algorithm altogether.

\item Using parallel processing [6] or distributed computing to compute the result of our algorithm faster.

\end{itemize}


\begin{figure}
  \centering
  \includegraphics[scale = 0.4]{graph.jpg}
  \caption{Comparison of $g(x) = x^3$ and $f(x) = x^{2.375}$} 
\end{figure}

\section{Results}

We will run our algorithm on several images and compare the results with the naïve blurring algorithm. We use grayscale images for comparison and also indicate the time taken by our algorithm on that image. We add Gaussian noise of $\sigma^2 = 0.01$ to every image.
\begin{center}
 \begin{tabular}{|c|c| c| c| c| c| } 
 \hline
 & Aerial & Barbara & Boats & Cameraman & Lena  \\ [0.5ex] 
 \hline
 PSNR (of noisy image) & 20.1697 & 20.1524 & 20.1176 & 20.3833 & 20.0523\\ 
 \hline
 PSNR (of blurry image) & 22.5269 & 22.9746 & 25.1432 & 22.2172 & \cellcolor{blue!25}27.4772 \\
 \hline
 PSNR (of our image) & \cellcolor{blue!25}24.3000 & \cellcolor{blue!25}24.1942 & \cellcolor{blue!25}25.7386 & \cellcolor{blue!25}23.6692 & 27.0973\\
 \hline
 MAE (of noisy image) & 0.0788 & 0.0787 & 0.0789 & 0.0757 & 0.0796\\
 \hline
 MAE (of blurry image) & 0.0531 & 0.0466 & 0.0363 & 0.0451 & \cellcolor{red!25}0.0273\\
 \hline
 MAE (of our image) & \cellcolor{red!25}0.0444 & \cellcolor{red!25}0.0424 & \cellcolor{red!25}0.0351 & \cellcolor{red!25}0.0415 & 0.0290\\
 \hline
 Time elapsed (in seconds) & 9.943808 & 26.510593 & 22.416332 & 8.281311 & 20.076168\\ 
 \hline
 Image resolution ($m \times n$) & $256 \times 256$ & $512 \times 512$ & $512 \times 512$ & $256 \times 256$ & $512 \times 512$\\
 \hline
 Actual iterations ($K'$) & 209 & 233 & 203 & 195 & 166\\
 \hline

\end{tabular}
\end{center}

A higher value of PSNR or a lower value of MAE indicate that the image is closer to the true image. \\

Also, we notice that in the image Lena, the PSNR and MAE that we get have better values when we only perform the Gaussian blur on the image and not our algorithm. This is justified by the fact that since the image Lena does not have many sharp edges, the blur only removes the noise in the image and does not affect the values of pixels in a noticeable manner. 

\begin{figure}
\begin{minipage}[t]{0.49\linewidth}
\centering
\includegraphics[width=\linewidth]{blurry_noisy_lena.jpg}

Blurred noisy Lena, PSNR = 27.4772 dB
\end{minipage}%
\hfill\vrule\hfill
\begin{minipage}[t]{0.49\linewidth}
\centering
\includegraphics[width=\linewidth]{denoised_lena.jpg}

(b) Denoised Lena, PSNR = 27.0973 dB
\end{minipage}
\caption{Comparison of the image Lena using our algorithm}
\end{figure}

% -----------------------------------

\begin{figure}
\begin{minipage}[t]{0.49\linewidth}
\centering
\includegraphics[width=\linewidth]{noisy_aerial.jpg}

Aerial, noisy, PSNR = 20.1697 dB
\end{minipage}%
\hfill\vrule\hfill
\begin{minipage}[t]{0.49\linewidth}
\centering
\includegraphics[width=\linewidth]{denoised_aerial.jpg}

(b) Aerial, denoised, PSNR = 24.3000 dB
\end{minipage}
\caption{Comparison of the image Aerial}
\end{figure}

\section{Conclusion}

We conclude that our algorithm successfully denoises images. We also add that this algorithm can be extended to use the ROF norm and the entire family of $\mathcal{L}^p$ [7] norms. Our algorithm performs best when given images that have sharp contrast in the edges. For example, we get the highest percentage improvement in PSNR with the image Aerial, which has several sharp edges. Therefore, we can use Primal-Dual Hybrid Gradient Methods for denoising images.

\section{Acknowledgements}

The author wishes to thank his mentor Dr. Stanislav Harizanov for introducing the problem and providing ideas towards an efficient solution. The author also thanks HSSIMI for giving him an opportunity to work on a fascinating research topic with a brilliant mentor.  Much of this work may not have been possible without the contributions of Dr. Harizanov.

\section{References}

[1] Jain, Anil K. Fundamentals of digital image processing. Prentice-Hall, Inc., 1989. \\

\hspace{-6mm}[2] Zhu, Mingqiang, and Tony Chan. "An efficient primal-dual hybrid gradient algorithm for total variation image restoration." UCLA CAM Report 34 (2008).\\

\hspace{-6mm}[3] Harizanov, Stanislav, Ivan Lirkov, and Ivan Georgiev. "Denoising 2D CT Radiographic Images." Proceedings of the International Conference on Numerical Methods for Scientific Computations and Advanced Applications (NMSCAA’16). 2016.\\

\hspace{-6mm}[4] Coppersmith, Don, and Shmuel Winograd. "Matrix multiplication via arithmetic progressions." Proceedings of the nineteenth annual ACM symposium on Theory of computing. ACM, 1987. \\

\hspace{-6mm}[5] Kovesi, Peter D. "MATLAB and Octave functions for computer vision and image processing." Centre for Exploration Targeting, School of Earth and Environment, The University of Western Australia (2000). \\

\hspace{-6mm}[6] Bräunl, Thomas, et al. Parallel image processing. Springer Science \& Business Media, 2013. \\

\hspace{-6mm}[7] Hardt, Robert, and Fang‐Hua Lin. "Mappings minimizing the Lp norm of the gradient." Communications on Pure and Applied Mathematics 40.5 (1987): 555-588.
\end{document}
