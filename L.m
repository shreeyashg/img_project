function m = L(sz)

m = zeros(sz, sz);

for i = 1:sz-1
    m(i, i) = -1.;
    m(i, i + 1) = 1.;
end

