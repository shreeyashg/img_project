function tensor = run(F, K)

maxvalue = double(max(max(F)));
F = double(F)/maxvalue;

sigma = 0.01;
noisy_F = imnoise(F, 'gaussian' , 0, 0.01);
blurry_F = blur_gaussian_2D(noisy_F, 2);
our = PDHGM(noisy_F, K, sigma, 0.5, 0.2, F);
our = full(our);
[peaksnr, snr] = psnr(noisy_F, F);
disp(peaksnr);
[peaksnr2, snr2] = psnr(blurry_F, F);
disp(peaksnr2);
[peaksnr3, sne3] = psnr(our, F);
disp(peaksnr3);

disp(mae(noisy_F, F));
disp(mae(blurry_F, F));
disp(mae(our, F));

figure; colormap gray; imagesc(F);
figure; colormap gray; imagesc(noisy_F);
figure; colormap gray; imagesc(blurry_F);figure; colormap gray; imagesc(our);

