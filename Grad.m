function Res = Grad(F)
% this program returns the length of the Gradient of an image F.

[m, n] = size(F);
% m = rows, n = columns
Res = zeros(m, n);

for i=1:m-1
    for j=1:n-1
        Res(i, j) = sqrt((double((F(i+1, j) - F(i, j))^2)) + (double((F(i, j+1) - F(i, j))^2)));
    end
end


for j = 1:n-1
    Res(m, j) = abs(F(m, j+1) - F(m, j));
end

for i = 1:m-1
    Res(i, n) = abs(F(i + 1, n) - F(i, n));
end



